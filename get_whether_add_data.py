import requests
import jdatetime
from bs4 import BeautifulSoup,Tag
from openpyxl import *
import re

now = jdatetime.datetime.now()
datetime = now.strftime("%Y-%m-%d %H:%M")
urls = ['https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AA%D8%B1%D8%A8%D8%AA-%D8%AD%DB%8C%D8%AF%D8%B1%DB%8C%D9%87.html?id=189',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AA%D8%B1%D8%A8%D8%AA-%D8%AC%D8%A7%D9%85.html?id=210',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%86%DB%8C%D8%B4%D8%A7%D8%A8%D9%88%D8%B1.html?id=184',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AA%D8%A7%DB%8C%D8%A8%D8%A7%D8%AF.html?id=17579',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%A8%D8%A7%D8%AE%D8%B1%D8%B2.html?id=17662',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AF%D9%85%D8%A7%D9%88%D9%86%D8%AF.html?id=332',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%85%D8%A7%D9%87-%D9%86%D8%B4%D8%A7%D9%86.html?id=158',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%B1%D8%B2%D9%86.html?id=336',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%82%D9%87%D8%A7%D9%88%D9%86%D8%AF.html?id=349',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%DA%AF%D9%86%D8%A8%D8%AF-%DA%A9%D8%A7%D9%88%D9%88%D8%B3.html?id=283',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%DA%A9%D9%84%D8%A7%D9%84%D9%87.html?id=261',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%85%DB%8C%D9%86%D9%88%D8%AF%D8%B4%D8%AA.html?id=17570']
wb = load_workbook('whether_data.xlsx')
ws = wb.active
data = [datetime]

def check_empty(s):
    if s == "-":
       return True
    else:
        return False
def process_data(s,pattern,processed_data):
    if (check_empty(s)):
        processed_data = "-"
    if s[0:3] == "صفر":
        processed_data = "۰"
    else:
        for m in re.findall(pattern, s):
            if pattern == float_pattern :
                processed_data = m
            else:
                processed_data = m[0]
    return processed_data


for url in urls:
    ### Try until success ###
    while True:
        try:
            resp = requests.get(url)
            break
        except requests.exceptions.RequestException:
            continue
    resp_txt = resp.text
    soup = BeautifulSoup(resp_txt, 'html.parser')
    city_name = soup.find('div', attrs={'class': 'station_blk_title'}).text[9:]
    ### get informations for excel ###
    # temp_data is tempratury data, we put all raw data in page to this
    temp_data = []
    # captions is captions that exists in the page, and always is stable
    captions = soup.find_all('span', attrs={'class': 'wet_s_wblk_caption'}) 

    for i in captions:
        # next_sibling is data for caption
        wblk_data = i.next_sibling
        # try except for when we does not have any data and
        # tag and we have Attribute Error
        try:
            while (not isinstance(wblk_data,Tag)):
                wblk_data = wblk_data.next_sibling        
        
            if(wblk_data.name != "span"):
                # sometimes, data is not exist, so we don't have a span tag, and we fill it with - .
                temp_data.append("-")
            else:
                temp_data.append(wblk_data.text)
        except AttributeError:
            temp_data.append("-")
    # j is step of loop
    j = 0
    # Pattern for finding tempreture
    tempt_pattern = r'([+-]?[\u06F0-\u06F90-9]+(\.[\u06F0-\u06F90-9]+)*)\s?°\s?([Cc])'
    # Pattern for finding float
    float_pattern = r'-?\d+\.?\d*'
    for s in temp_data:
        # final data is proccessed data
        final_data = "null"
        # add vapor and raining
        if j == 1 or j == 3:
            final_data = process_data(s,float_pattern,final_data)
        # add max and min tempt
        if j == 5 or j == 6:
            final_data = process_data(s, tempt_pattern,final_data)
        if final_data != "null":
            data.append(final_data)
        j += 1
    # this append is for space between cities in excel
    data.append("")
### add data to excel  file ###
ws.append(data)
wb.save("whether_data.xlsx")
