import requests
import jdatetime
from bs4 import BeautifulSoup
from openpyxl import *
import re

now = jdatetime.datetime.now()
datetime = now.strftime("%Y-%m-%d %H:%M")
urls = ['https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AA%D8%B1%D8%A8%D8%AA-%D8%AD%DB%8C%D8%AF%D8%B1%DB%8C%D9%87.html?id=189',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AA%D8%B1%D8%A8%D8%AA-%D8%AC%D8%A7%D9%85.html?id=210',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%86%DB%8C%D8%B4%D8%A7%D8%A8%D9%88%D8%B1.html?id=184',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AA%D8%A7%DB%8C%D8%A8%D8%A7%D8%AF.html?id=17579',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%A8%D8%A7%D8%AE%D8%B1%D8%B2.html?id=17662',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%AF%D9%85%D8%A7%D9%88%D9%86%D8%AF.html?id=332',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%85%D8%A7%D9%87-%D9%86%D8%B4%D8%A7%D9%86.html?id=158',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D8%B1%D8%B2%D9%86.html?id=336',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%82%D9%87%D8%A7%D9%88%D9%86%D8%AF.html?id=349',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%DA%AF%D9%86%D8%A8%D8%AF-%DA%A9%D8%A7%D9%88%D9%88%D8%B3.html?id=283',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%DA%A9%D9%84%D8%A7%D9%84%D9%87.html?id=261',
'https://www.irimo.ir/far/wd/701-%D9%BE%DB%8C%D8%B4-%D8%A8%DB%8C%D9%86%DB%8C-%D9%88%D8%B6%D8%B9-%D9%87%D9%88%D8%A7%DB%8C-%D9%85%DB%8C%D9%86%D9%88%D8%AF%D8%B4%D8%AA.html?id=17570']

wb=Workbook()
# wb = load_workbook('whether_data.xlsx')
ws = wb.active
data = []
### Create Header ###
s = ["کمینه دما","بیشینه دما","میزان بارندگی","میزان رطوبت",""]
s = s[::-1]
for y in range(0,60):
  ws.cell(row=2 , column=y+1).value = s[y%5]
wb.save("whether_data.xlsx")
data.append(datetime)
x = 2
for url in urls:
    resp = requests.get(url)
    resp_txt = resp.text
    soup = BeautifulSoup(resp_txt, 'html')
    city_name = soup.find('div', attrs={'class': 'station_blk_title'}).text[9:]
    ### add city name for header ###
    ws.cell(row=1, column=x).value = city_name
    x += 5
    ### get informations for excel ###


wb.save("whether_data.xlsx")



### reverse array ###
# data = data[::-1]
### add data to excel  file ###

# ws.append(data)
wb.save("whether_data.xlsx")

########### create html file ###############

# page = open("page.html","r")
# block_forecast = str(soup.find('div', attrs={'class': 'block_forecast'}))
# ChdTwoColumnsLeft = str(soup.find('div', attrs={'class': 'ChdTwoColumnsLeft'}))
#
# file = open("%s.html"%name,"w")
#
# with page as f:
#     lines = f.readlines()
#     with file as f1:
#         f1.writelines(lines)
#
# lines = open("%s.html"%name).readlines()
# lines[184] = block_forecast
# open("%s.html"%name,'w').write(''.join(lines))
#
# lines = open("%s.html"%name).readlines()
# lines[179] = ChdTwoColumnsLeft
# open("%s.html"%name,'w').write(''.join(lines))
# file.close()
